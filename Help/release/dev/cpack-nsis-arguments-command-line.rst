cpack-nsis-arguments-command-line
---------------------------------

* The :cpack_gen:`CPack NSIS Generator` gained two new variables
  :variable:`CPACK_NSIS_EXECUTABLE_PRE_ARGUMENTS` and
  :variable:`CPACK_NSIS_EXECUTABLE_POST_ARGUMENTS`
  to provide arguments to the nsis executable invocation.
